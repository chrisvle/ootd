Rails.application.routes.draw do
  root to: 'home#index'
  devise_for :users
  devise_for :models
  resources :users
  resources :outfits
  resources :items
  get 'outfits/new', to:'outfits#new', as: 'new'
  post '/rate' => 'rater#create', :as => 'rate'

end
