# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Make Outfit
%w(Interview Snow Beach).each do |name|
  Outfit.create name: name, rating: 100
end

# Make other users
%w(Chris Helen Tim Kenny Jeremy ).each do |name|
  User.create name: name, email: name+"@ootd.com", password: 'password'
end

# Make apparel 
%w(Shirt Pants Parka).each do |name|
  Apparel.create name: name
end