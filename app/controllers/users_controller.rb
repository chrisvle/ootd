class UsersController < ApplicationController
  before_filter :authenticate_user!

  def index
    @users = User.all
    @outfits = Outfit.all
  end

  def show
    @user = current_user
    @outfits = Outfit.where(user_id:current_user)
  end

end
