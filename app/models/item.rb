class Item < ActiveRecord::Base
  belongs_to :outfit
  mount_uploader :image, ImageUploader
end
