class Outfit < ActiveRecord::Base
	  # ratyrate_rateable "Hype level"
		has_many :items
		ratyrate_rateable 'Appeal'
		ratyrate_rater

		belongs_to :user
		validates :name, presence: true, uniqueness: true
end
